<?php

namespace App\Http\Controllers;

use App\Models\Kalkulator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KalkulatorController extends Controller
{
    /**
     * Menampilkan halaman awal
     * 
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $perhitungan = Kalkulator::orderBy('created_at', 'desc')->get();
        
        return view('kalkulator.kalkulator', ['perhitungan' => $perhitungan]);
    }

    /**
     * Menambahkan hasil perhitungan ke dalam database dan melakukan operasi aritmatik
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function hitung(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'bil1' => 'required|regex:/^\d+(,\d+)?$/',
            'bil2' => 'required|regex:/^\d+(,\d+)?$/',
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        $bil1 = $request->bil1;
        $bil2 = $request->bil2;
        $operator = $request->operator;
        $hasil = 0;
        
        if($operator == '+'){
            $hasil = $bil1 + $bil2;
        }elseif($operator == '-'){
            $hasil = $bil1 - $bil2;
        }elseif($operator == '*'){
            $hasil = $bil1 * $bil2;
        }elseif($operator == '/'){
            $hasil = $bil1 / $bil2;
        }elseif($operator == '%'){
            $hasil = $bil1 % $bil2;
        }
        // dd($hasil);
        Kalkulator::create([
            'bil1' =>$bil1,
            'bil2' =>$bil2,
            'operasi' =>$operator,
            'hasil' =>$hasil,
        ]);
        
        return redirect()->back();
    }
}
