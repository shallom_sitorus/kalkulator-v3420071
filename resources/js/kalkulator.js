// Ambil elemen-elemen tampilan
const operand1Input = document.getElementById('bil1');
const operand2Input = document.getElementById('bil2');
const operatorInput = document.getElementById('operator');
const resultOutput = document.getElementById('result');

// Tambahkan event listener untuk saat tombol "Hitung" ditekan
document.getElementById('calculateBtn').addEventListener('click', function(event) {
    event.preventDefault();

    // Ambil nilai operand dan operator dari input
    const operand1 = parseFloat(operand1Input.value);
    const operand2 = parseFloat(operand2Input.value);
    const operator = operatorInput.value;

    // Lakukan perhitungan berdasarkan operator
    let result;
    switch (operator) {
        case '+':
            result = operand1 + operand2;
            break;
        case '-':
            result = operand1 - operand2;
            break;
        case '*':
            result = operand1 * operand2;
            break;
        case '/':
            result = operand1 / operand2;
            break;
        default:
            result = 'Invalid operator';
            break;
    }

    // Perbarui elemen tampilan dengan hasil perhitungan
    resultOutput.textContent = `Hasil: ${result}`;
});
