<div class="row g-3">
    <div class="col-md-6">
      <div class="row">
        <label class="col-sm-3 col-form-label text-sm-end" for="collapsible-fullname">Full Name</label>
        <div class="col-sm-9">
          <input type="text" id="collapsible-fullname" class="form-control" placeholder="John Doe" />
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="row">
        <label class="col-sm-3 col-form-label text-sm-end" for="collapsible-phone">Phone No</label>
        <div class="col-sm-9">
          <input type="text" id="collapsible-phone" class="form-control phone-mask" placeholder="658 799 8941" aria-label="658 799 8941" />
        </div>
      </div>
    </div>
</div>