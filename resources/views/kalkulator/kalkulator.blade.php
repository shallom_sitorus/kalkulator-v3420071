<!-- resources/views/calculator.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="container">
        <h5>Kalkulator</h5>
        @if (Session::has('error'))
        <div class="invalid-feedback">
            {{ session('error') }}
        </div>
        @endif
        <form action="/hitung" method="POST">
            @csrf
            <div class="row g-3">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-sm-9">
                            <input type="text" name="bil1" id="bil1" class="form-control @error('bil1') is-invalid @enderror" placeholder="angka pertama" required/>
                            @error('bil1')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-sm-9">
                            <select class="form-control" id="operator" name="operator" required>
                                <option value="+">+</option>
                                <option value="-">-</option>
                                <option value="*">*</option>
                                <option value="/">/</option>
                                <option value="%">%</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-sm-9">
                            <input type="text" name="bil2" id="bil2" class="form-control @error('bil2') is-invalid @enderror" placeholder="angka kedua" required/>
                            @error('bil2')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="row g-3">
                <div class="col-md-4 offset-md-8">
                    <div class="mt-2 text-end">
                        <button type="submit" id="calculateBtn" class="btn btn-success">Hitung</button>
                    </div>
                </div>
            </div>
        </form>
        <div class="container">
            <h5>Riwayat Hasil Perhitungan</h5>
            @if (count($perhitungan) > 0)
                <table class="table text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Angka 1</th>
                            <th>Operasi</th>
                            <th>Angka 2</th>
                            <th>Hasil</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($perhitungan as $hitung)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $hitung->bil1 }}</td>
                                <td>{{ $hitung->operasi }}</td>
                                <td>{{ $hitung->bil2 }}</td>
                                <td>{{ $hitung->hasil }}</td>
                                <td>
                                    <button class="btn btn-primary hitunglagi" data-bil1="{{ $hitung->hasil }}">Hitung Kembali</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p>Tidak ada riwayat perhitungan.</p>
            @endif
        </div>
    </div>
    <script>
        const hitlagibutton = document.getElementsByClassName('hitunglagi');
        const bil1Input = document.getElementById('bil1');

        //menggunakan for untuk mengembalikan jumlah elemen dalam array hitlagibutton
        for (let i = 0; i < hitlagibutton.length; i++) {
            hitlagibutton[i].addEventListener('click', function () {
                const bil1Value = this.getAttribute('data-bil1');
                bil1Input.value = bil1Value;
            });
        }
    </script>
@endsection
